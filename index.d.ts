export class StateProducer {
    constructor(kafkaConfig: object, topics: Array<String>);
    on(eventName: "loaded", callback: (topicMap: Map) => any): void;
    merge(topic: string, key: string, record: object): void;
    set(topic: string, key: string, record: object): void;
    delete(topic: string, key: string): void;
    deleteState(topic: string): void;
    get(topic: string, key: string): object;
    getState(topic: string): Map;
}

export class StateConsumer {
    constructor(kafkaConfig: {
        broker: string,
        groupId: string,
        clienId: string,
        schemaRegister: string,
        stageTopicLoading: boolean,
        maxAge: bigint,
        commitOffsetsOnOverAge: true
    }, topics: Array<String>);
    on(eventName: "loaded", callback: (topicMap: Map) => any): void;
    on(eventName: "update", callback: (topic: string, key: string) => any): void;
    //on(eventName: "delete", callback: (topic: string, key: string) => any): void;
    close(): void;
    get(topic: string, key: string): object;
    getState(topic: string): Map;
    getStateStore(): Map;
}

export class Consumer {
    constructor(kafkaConfig: {
        broker: string,
        clienId: string,
        schemaRegister: string,
        commitOffsets: boolean,
        
        disconnectTopicAfterLoad: boolean,
        maxAge: bigint,// Seconds
        commitOffsetsOnOverAge: true
    }, topics: Array<String>);
    on(eventName: "message", callback: (topic: string, key: string, message: object,timestamp:Date) => any): void;
    on(eventName: "loaded", callback: (topics: Array<String>) => any): void;
    close(): void;
    addTopic(topic: string): void;
    removeTopic(topic: string): void;

}

export class Producer {
    constructor(kafkaConfig: object);
    on(eventName: "connected", callback: () => any): void;
    send(topic: string, message: string, key: string, autoCreationConfig: object): Promise;
    close(): void;

}