// This demo stores an application state on cafka as json based on 3 external states that are encoded in avro.

const { StateProducer, StateConsumer } = require("./index");

// Load application state
var producer = new StateProducer({
    broker: "localhost:9092",
    clientId: "test-kafka-connect1"

}, ["_state.demo"]);

// Process external state when application state is loaded. 
producer.on("loaded", (topicMap) => {
    console.log("State loaded!");

    //enable next line to clear application state from kafka
    //producer.deleteState("_state.test");

    var consumer = new StateConsumer({
        broker: "localhost:9092",
        clientId: "test-kafka-connect2",
        schemaRegister: "localhost:8081",
        stageTopicLoading: false, // Indicates if topics are read one by one up tu current or in paralell. Usefull if state transformation is needen in particular order.
        commitOffsets: true // As this application has persisted state we only need to read from last commit.
    }, ["entity.vehicle.position", "entity.vehicle.status", "entity.vehicle.key"]);

    //Process new updates from external state
    consumer.on("message", (message) => {

        if (message.topic == "entity.vehicle.position") record.position = message.value;
        else if (message.topic == "entity.vehicle.key") record.key = message.value;
        else if (message.topic == "entity.vehicle.status") record.status = message.value;

        // Merge update with existing application state and publish state to Kafka if changed.
        producer.merge("_state.demo", message.key, record)
    })

    // Use internal state for something when loded up to current on all external state topics;
    consumer.on("loaded", () => {

        // Output number of vehicles every 10 seconds
        setInterval(() => {
            // Internal state of the producer is a Map()
            console.log("Number of vehicles: " + producer.getState.size);
        }, 10000);

    });

})



