// This class maintains an in memory state that is populated from a kafka topic and published to the same topic when changed

const EventEmitter = require('events');
const NConsumer = require("sinek").NConsumer;

function convertIfJSON(str) {
    var result;
    try {
        result = JSON.parse(str);
    } catch (e) {
        result = str;
    }
    return result;
}

class kafkaConsumer extends EventEmitter {
    constructor(kafkaConfig, topics) {
        super(); //must call super for "this" to be defined.
        this._loaded = false;
        //Create consumer config


        this._kafkaConsumerConfig = {
            noptions: {
                "metadata.broker.list": kafkaConfig.broker,
                "group.id": kafkaConfig.clientId,
                "client.id": kafkaConfig.clientId,
                "api.version.request": true,
                "auto.offset.reset": "beginning",
                "socket.keepalive.enable": true,
                "enable.auto.commit": false,
                "enable.auto.offset.store": false
            },
            tconf: {
                "auto.offset.reset": "beginning"
            }
        };

        if (!Array.isArray(topics)) throw new Error("Consumer: Input parameter topics need to be an array")

        this._stageLoading = kafkaConfig.stageTopicLoading || false;;
        this._remainingTopics = topics;
        this._topics = [];
        this._originalTopics=topics.slice(0); // Slice is to clone the array
        /*if (this._stageLoading) {
            this._topics = [this._remainingTopics.shift()];
        }
        else this._topics = topics;*/
        this._schemaRegisterURL = kafkaConfig.schemaRegister || null;
        this._commitOffsets = kafkaConfig.commitOffsets || false;
        this._disconnectAfterLoad = kafkaConfig.disconnectTopicAfterLoad || false;
        this._maxAge = kafkaConfig.maxAge || 0;
        this._commitOffsetsOnOverAge = kafkaConfig.commitOffsetsOnOverAge || false;

        this._closeConnection = null;
        this._close = false;
        this._loaded = false;
        this._topicOffsets = new Map();
        this._connected = false;


        this._consumer = new NConsumer(this._topics, this._kafkaConsumerConfig);


        var self = this;

        consume();


        async function consume(processRecord) {
            console.log("Consumer: Ready to consume");

            try {
                //Setup schema registry
                var registry = null;
                if (self._schemaRegisterURL) {
                    registry = require('avro-schema-registry')(self._schemaRegisterURL);
                }

                const batchSize = 5000;
                const batchOptions = {
                    batchSize: batchSize, // decides on the max size of our "batchOfMessages"
                    commitEveryNBatch: 1, // will be ignored
                    concurrency: 1, // will be ignored
                    commitSync: false, // will be ignored
                    noBatchCommits: true, // important, because we want to commit manually
                    manualBatching: true, // important, because we want to control the concurrency of batches ourselves
                    sortedManualBatch: true, // important, because we want to receive the batch in a per-partition format for easier processing
                };

                const consumer = self._consumer

                await consumer.connect();


                self._closeConnection = function () {
                    console.log("Consumer: Manualy disconnected from kafka");
                    consumer.commitLocalOffsetsForTopic(topic);
                    consumer.close();
                };


                var topicOffsets = self._topicOffsets;

                if (self._stageLoading) {
                    self.addTopic(self._remainingTopics.shift())
                } else {
                    self._remainingTopics.forEach((topic) => {
                        self.addTopic(topic);
                    })
                    self._remainingTopics = [];
                }

                // Reporting on load progress with delayed start
                const reportingDelay = 20;
                const reportingInterval = 10;
                function reportProgress() {
                    if (self._connected) {
                        topicOffsets.forEach((offset, topic) => {
                            if (!offset.done && (offset.highOffset >= 0)) {
                                var diff = (offset.highOffset - offset.currentOffset);
                                if (diff < 0) diff = 0;
                                console.log("Consumer: Progress loading " + topic + " - Remaining/Processed: " + diff + "/" + offset.recordsProcessed);
                            }
                        });
                        //Repeat if not loaded
                    }
                    if (!self._loaded) setTimeout(reportProgress, reportingInterval * 1000);
                }
                // Start reporing delayed
                setTimeout(reportProgress, reportingDelay * 1000);


                // Check loading of topics
                function checkLoading() {
                    if (self._connected) {
                        consumer.getLagStatus().then((offsetStatuses) => {
                            var emptyTopics=false;
                            if(offsetStatuses.length==0) emptyTopics=true;

                            offsetStatuses.forEach((status) => {
                                var topicOffset = self._topicOffsets.get(status.topic);
                                topicOffset.highOffset = status.detail.highOffset
                                if (status.detail.comittedOffset > topicOffset.currentOffset) topicOffset.currentOffset = status.detail.comittedOffset;
                                if (status.highDistance == 0) topicOffset.currentOffset = topicOffset.highOffset;
                            });

                            var doneLoading = true;
                            self._topicOffsets.forEach((topicOffset, topic) => {
                                //offsetStatuses.forEach((status) => {
                                //var topicOffset = self._topicOffsets.get(status.topic);
                                if(emptyTopics) topicOffset.done=true;
                                if (!topicOffset.done) {
                                    // Skip check if no progress on current offset 
                                    if (topicOffset.highOffset == topicOffset.lowOffset) doneLoading = true;
                                    else if (topicOffset.currentOffset == topicOffset.lowOffset) doneLoading = false;
                                    else {
                                        var lastOffset = topicOffset.currentOffset;
                                        var highOffset = topicOffset.highOffset;
                                        var lag = highOffset - lastOffset;

                                        var topicLoaded = false;
                                        if (self._disconnectAfterLoad && (lag == 0)) {
                                            topicLoaded = true;
                                        } else if (lag < batchSize) {
                                            topicLoaded = true;
                                        } else {

                                        }
                                        if (topicLoaded) {
                                            console.log("Consumer: Loaded up to current offset on topic " + topic + " - " + topicOffset.recordsProcessed + " records processed");
                                            topicOffset.done = true;
                                            if (self._disconnectAfterLoad) self.removeTopic(topic);
                                            if (self._stageLoading && (self._remainingTopics.length > 0)) {
                                                doneLoading = false;
                                                self.addTopic(self._remainingTopics.shift())
                                            }

                                        } else doneLoading = false;
                                    }
                                }
                            });

                            if (doneLoading && !self._loaded) {
                                self._loaded = true;
                                self.emit('loaded', self._originalTopics);
                            } else setTimeout(checkLoading, 1000);

                        });
                    } else setTimeout(checkLoading, 2000);
                }
                // Start load checking
                checkLoading();

                // Set connected after one minute. Handles initially empty topics
                setTimeout(()=>{
                    self._connected = true;
                },60000)


                consumer.consume(async (batchOfMessages, callback) => {
                    // Run if first batch
                    if (!self._connected) {
                        self._connected = true;
                        console.log("Consumer: Recieving messages from broker");
                    }
                    const topicPromises = Object.keys(batchOfMessages).map(async (topic) => {
                        var topicOffset = self._topicOffsets.get(topic);

                        // parallel processing on partition level
                        const partitionPromises = Object.keys(batchOfMessages[topic]).map((partition) => {

                            // sequential processing on message level (to respect ORDER)
                            const messages = batchOfMessages[topic][partition];
                            messages.forEach(async (message) => {

                                topicOffset.currentOffset = message.offset;

                                var tsCutoff = Date.now() - (self._maxAge * 1000);
                                if ((message.timestamp > tsCutoff) || (self._maxAge <= 0)) {
                                    topicOffset.recordsProcessed++;
                                    // Get message key
                                    var key = null;
                                    if (message.key) key = message.key.toString();

                                    var msg = null;
                                    if (message.size>0) {
                                        if (message.value.readUInt8(0) !== 0) {
                                            msg = message.value.toString();
                                            // Convert string to javascript object if JSON
                                            msg = convertIfJSON(msg);
                                        } else {
                                            if (registry) {
                                                // Decode Avro message
                                                msg = await registry.decode(message.value)
                                            }
                                            else throw new Error("No registry set up")
                                        }
                                    }
                                    self.emit('message', { topic: topic, key: key, value: msg, timestamp: new Date(message.timestamp) });
                                }
                            });
                            return Promise.resolve();
                        });



                        // Wait until all partitions of this topic are processed and commit its offset if set
                        await Promise.all(partitionPromises);
                        // Commit offsets if set
                        if (self._commitOffsets) await consumer.commitLocalOffsetsForTopic(topic);
                        // Commit offsets for messages that are over age if set
                        else if(self._commitOffsetsOnOverAge){
                            topicOffsets.forEach((topicOffset, topic, ) => {
                                if (topicOffset.recordsProcessed == 0) {
                                    consumer.commitLocalOffsetsForTopic(topic);
                                }
                            });
                        }
                    });

                    await Promise.all(topicPromises);
                    // callback still controlls the "backpressure"
                    // as soon as you call it, it will fetch the next batch of messages
                    if (self._close) {
                        console.log("Consumer: Close called");
                        consumer.close();
                    }
                    else callback();

                }, false, false, batchOptions);

                var closed=false;

                const shutdownConsumer = function () {
                    if(!closed){
                        consumer.close();
                        console.log("Consumer: Disconnected from kafka and exiting");
                        process.exit();
                    }
                };

                process.on('exit', shutdownConsumer);
                process.on('SIGTERM', shutdownConsumer);
                process.on('SIGINT', shutdownConsumer);
            } catch (error) {
                console.log(error)
                console.log("Exiting");
                shutdownConsumer();
            }
        }
    }

    close() {
        this._close = true;
    }

    addTopic(topic) {
        if (!this._topics.includes(topic)) {

            
            var self = this;

            //this._topicOffsets.set(topic, { lowOffset: -1, highOffset: -1, currentOffset: -1, done: false, recordsProcessed: 0 });

            this._loaded = false;
            this._consumer.getOffsetForTopicPartition(topic, 0).then((offsets) => {
                self._topicOffsets.set(topic, { lowOffset: offsets.lowOffset, highOffset: offsets.highOffset, currentOffset: offsets.lowOffset, done: false, recordsProcessed: 0 });
                self._topics.push(topic);
                self._consumer.adjustSubscription(self._topics);
                console.log("Consumer: Added topic (" + topic + ")");
                if (self._stageLoading) {
                    setTimeout(() => {
                        var offset = self._topicOffsets.get(topic);
                        if (offset.currentOffset == offset.lowOffset) {
                            offset.currentOffset = offset.highOffset;
                            console.log("Consumer: No activity on topic " + topic + ". Processing next topic if any");
                        }
                    }, 60000);
                }

            })
        } else throw new Error("Consumer: Trying to add a topic that is already consumed")
    }

    removeTopic(topic) {
        var index = this._topics.indexOf(topic);
        if (index !== -1) {
            this._topics.splice(index, 1);
            this._topicOffsets.get(topic).done = true;
            this._consumer.adjustSubscription(this._topics);
            console.log("Consumer: Removed topic (" + topic + ")");
        }
        else throw new Error("Consumer: Trying to remove a topic that is not consumed")

    }
}


module.exports = kafkaConsumer;