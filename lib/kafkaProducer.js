// This class sends messages without a key to kafka topics

const EventEmitter = require('events');
const NProducer = require("sinek").NProducer;



class kafkaProducer extends EventEmitter {
    constructor(kafkaConfig) {
        super(); //must call super for "this" to be defined.
        //Create consumer and producer configs
        this._kafkaProducerConfig = {
            noptions: {
                "metadata.broker.list": kafkaConfig.broker,
                "client.id": kafkaConfig.clientId,
                "compression.codec": "none",
                "socket.keepalive.enable": true,
                "api.version.request": true,
                "queue.buffering.max.ms": 100,
                "batch.num.messages": 200
            },
            tconf: {
                "request.required.acks": 1
            }
        };
        this._broker = kafkaConfig.broker;
        this._connected = false;
        this._brokerTopics = new Map();

        var self = this;

        //Setup kafka Producer
        this._producer = new NProducer(this._kafkaProducerConfig, null, 1/*number of partitions*/);
        this._producer.on("error", error => console.error(error));

        this._producer.connect().then(() => {
            self._producer.getMetadata().then((rawBrokerTopics) => {
                rawBrokerTopics.raw.topics.forEach(topic => {
                    self._brokerTopics.set(topic.name, topic.partitions.length);
                })
                console.log("Producer: Connected");
                self.emit('connected');
            })
        })

    }

    send(topic, message, key,autoCreationConfig) {
        if (this._brokerTopics.get(topic)) {
            if(message) message=JSON.stringify(message);
            else message="";
            return this._producer.send(topic, message , 0, key.toString());
        } else throw new Error("Producer: Not sending message. Topic " + topic + " does not exist on broker " + this._broker);
    }

    close(){
        return this._producer.close();
    }
}


module.exports = kafkaProducer;