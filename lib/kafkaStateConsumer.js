// This class maintains an in memory state that is populated from a kafka topic and published to the same topic when changed

const EventEmitter = require('events');
const diff = require("deep-object-diff").diff;
//const loki =require("lokijs");



class kafkaStateConsumer extends EventEmitter {
    constructor(kafkaConfig, topics) {
        super();

        this._topics = topics;
        this._topicMap = new Map();
        this._maxAge = kafkaConfig.maxAge || 0;
        this._disconnectAfterLoad = kafkaConfig.disconnectTopicAfterLoad || false;
        //this._loaded = false;

        var self = this;

        //Setup topic maps for in-memory state
        this._topics.forEach(function (topic) {
            self._topicMap.set(topic, new Map());
        });
        console.log("StateConsumer: In-memory structures ready");

        const { Consumer } = require("../index");

        // Override settings for state consuming
        kafkaConfig.commitOffsets = false;
        if (kafkaConfig.disconnectTopicAfterLoad===undefined) kafkaConfig.disconnectTopicAfterLoad = false;
        kafkaConfig.commitOffsetsOnOverAge = true;

        this.consumer = new Consumer(kafkaConfig, topics);

        this.consumer.on("loaded", (topics) => {
            topics.forEach(function (topic) {
                console.log("StateConsumer: Loaded until current. Got " + self._topicMap.get(topic).size + " record(s) from " + topic);
            });

            
            if(!this._disconnectAfterLoad) console.log("StateConsumer: Continuing to process incomming changes");
            if(self._maxAge!=0) {
                clean();
                console.log("StateConsumer: Running cleaner to continously remove records that are older than maxAge");
            }
            self.emit('loaded', self._topicMap);
        })

        this.consumer.on("message", (message) => {
            if (message.value) {
                var oldRecord = self._topicMap.get(message.topic).get(message.key);
                var changes = { dummy: 1 };
                if (oldRecord) changes = diff(oldRecord, message);
                if (Object.keys(changes).length != 0) {
                    self._topicMap.get(message.topic).set(message.key, message);
                    self.emit('update', message);
                }
            } else {
                self._topicMap.get(message.topic).delete(message.key);
                self.emit('update', message);
            }
        })


        // Cleaner
        var topicEntries;
        var currentTopic;
        var recordEntries;
        var currentRecord;
        var count = 0;
        var lastTopic;
        function clean() {
            var callbackDelay=10;
            if (!topicEntries) {
                topicEntries = self._topicMap.entries();
                currentTopic = topicEntries.next();
                recordEntries = currentTopic.value[1].entries();
                currentRecord = recordEntries.next();
            }
            if (!currentRecord.done) {
                //console.log(currentRecord.value[1].key)
                var cutOff = new Date(new Date() - self._maxAge*1000+3600000);
                var timestamp = currentRecord.value[1].timestamp;
                if (timestamp < cutOff) {
                    var topic = currentRecord.value[1].topic;
                    var key = currentRecord.value[1].key;
                    //console.log("Delete: " + currentRecord.value[1].key)
                    self.emit('update', {topic:topic,key:key,value:null,timstamp:new Date()});
                    self._topicMap.get(topic).delete(key);
                    currentRecord = recordEntries.next();
                    count++;
                    lastTopic=topic;
                } else currentRecord.done=true;

                

            } else {
                if(count!=0) console.log("StateConsumer: Removed " + count +" record(s) from "+lastTopic+" state due to age")
                count = 0;
                currentTopic = topicEntries.next();
                if (currentTopic.done) {
                    topicEntries = self._topicMap.entries();
                    currentTopic = topicEntries.next();
                    // 20 seconds delay between cleaning cycles
                    callbackDelay=20*1000;
                }
                recordEntries = currentTopic.value[1].entries();
                currentRecord = recordEntries.next();
            }

            setTimeout(clean,callbackDelay);
        }

    }

    close() {
        this.consumer.close();
    }

    get(topic, key) {
        return this._topicMap.get(topic).get(key);
    }

    getState(topic) {
        return this._topicMap.get(topic);
    }

    getStateStore() {
        return this._topicMap;
    }
}


module.exports = kafkaStateConsumer;