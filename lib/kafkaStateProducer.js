// This class maintains an in memory state that is populated from a kafka topic and published to the same topic when changed

const EventEmitter = require('events');
const Producer = require("./kafkaProducer");
const diff = require("deep-object-diff").diff;
const deepmerge = require("deepmerge");
//var clone = require("clone");


class kafkaStateProducer extends EventEmitter {
    constructor(kafkaConfig, topics, autoCreationConfig) {
        super(); //must call super for "this" to be defined.
        //Create consumer and producer configs


        //Setup topic maps for in-memory state
        this._topics = topics;
        this._topicMap;
        this._loaded = false;



        var self = this;

        //console.log("In-memory structures ready");

        const { StateConsumer } = require("../index");

        // Override settings for state consuming
        kafkaConfig.commitOffsets = false;
        kafkaConfig.disconnectTopicAfterLoad = true;
        kafkaConfig.commitOffsetsOnOverAge = false;
        kafkaConfig.maxAge = null;
        kafkaConfig.disconnectTopicAfterLoad = true;


        this.consumer = new StateConsumer(kafkaConfig, topics);
        this._topicMap = this.consumer.getStateStore();
        console.log("StateProducer: Loading state");

        this.consumer.on("loaded", (topics) => {
            console.log("StateProducer: State Loaded. Activating producer");

            setupProducer();
        })

        this.consumer.on("message", (message) => {
            self.emit('update', message);
        });



        //Setup kafka Producer
        function setupProducer() {
            self._producer = new Producer({ broker: kafkaConfig.broker, clienId: kafkaConfig.clientId });
            self._producer.on("connected", () => {
                self._loaded = true;
                console.log("StateProducer: Ready to change state");
                self.emit('loaded', self._topicMap);
            })
        }
    }

    merge(topic, key, record) {
        var oldRecord = this._topicMap.get(topic).get(key);
        var newRecord = record;
        const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray
        if (oldRecord) newRecord = deepmerge(oldRecord.value, record,{ arrayMerge: overwriteMerge });
        this.set(topic, key, newRecord);
    }

    set(topic, key, record) {
        if (this._loaded) {
            var oldRecord = this._topicMap.get(topic).get(key);
            var changes = { dummy: 1 };
            if (oldRecord) {
                changes = diff(oldRecord.value, record);
            }
            if (Object.keys(changes).length != 0) {
                this._topicMap.get(topic).set(key, {topic:topic,key:key,value:record,timestamp:new Date()});
                this._producer.send(topic, record, key);
            }
        } else throw new Error("StateProducer: Not ready to produce state")
    }

    delete(topic, key) {
        var record = this._topicMap.get(topic).get(key);
        if (record) {
            this._topicMap.get(topic).delete(key);
            this._producer.send(topic,null, key);
        }
    }

    deleteState(topic) {
        this._topicMap.get(topic).forEach((value, key) => {
            this.delete(topic, key);
        })
    }

    get(topic, key) {
        return this._topicMap.get(topic).get(key);
    }

    getState(topic) {
        return this._topicMap.get(topic);
    }

    getStateStore() {
        return this._topicMap;
    }
}


module.exports = kafkaStateProducer;